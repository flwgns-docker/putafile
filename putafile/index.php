<?php

    define('CURRENT_PATH', dirname(__FILE__));
    define('CONFIG_PATH', CURRENT_PATH.'/../config.php');
    define('TEMPLATES_PATH', CURRENT_PATH.'/templates');

    if (!file_exists(CONFIG_PATH)) {
        echo("PutAFile was not initialized.");
        exit(1);
    } else {
        include CONFIG_PATH;
    }

    session_start();

    if (!$_SESSION['who']) {
        header("Location: /login/");
    }

    if (isset($_POST["submit"])) {
        $url = $_POST["url"];

        $url_headers = @get_headers($url);
        if(!$url_headers || strpos($url_headers[0], '404')) {
            echo "Could not find anything at $url.";
        }

        $fetch = copy($url, PUT_FILE_PATH);
        if (!fetch) {
            echo("Failed to fetch $url.");
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <?php include TEMPLATES_PATH.'/meta.php' ?>
    <title>PutAFile</title>

    <?php include TEMPLATES_PATH . '/css.php' ?>
</head>
<body>
    <?php include TEMPLATES_PATH.'/nav.php' ?>

    <form class="form-signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Mettre à jour le VPN</h1>
        <p>Coller ci-dessous le lien vers un ficher OpenVPN de VPNGate pour mettre à jour le VPN.</p>
        <label for="url">URL du fichier</label>
        <input type="text" id="url" name="url" class="form-control" placeholder="https://exemple/de/lien" required autofocus>
        <hr>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Envoyer</button>
    </form>

    <?php include TEMPLATES_PATH.'/footer.php' ?>
</body>
</html>
