<nav>
    <div>
        <a href="/"><i class="fas fa-home"></i></a>
        <a href="/settings/"><i class="fas fa-cog"></i></a>
    </div>

    <div>
        <?php if($_SESSION["who"]) { ?>
        <a href="/logout/"><i class="fas fa-sign-out-alt"></i></a>
        <?php } ?>
    </div>
</nav>
