<?php
    session_start();

    if ($_SESSION['who']) {
        unset($_SESSION["who"]);
        session_destroy();
        header("Location: /");
    } else {
        header("Location: /");
    }
?>