<?php

    session_start();
    if ($_SESSION['who'] !== "super") {
        header("Location: /super/");
    }

    define('CURRENT_PATH', dirname(__FILE__));
    define('CONFIG_PATH', CURRENT_PATH.'/../../config.php');
    define('TEMPLATES_PATH', CURRENT_PATH.'/../templates');
    define('UTILS_PATH', CURRENT_PATH.'/../assets/php/utils.php');

    if (!file_exists(CONFIG_PATH)) {
        echo("PutAFile was not initialized.");
        exit(1);
    } else {
        include CONFIG_PATH;
    }

    include UTILS_PATH;

    if (isset($_POST["submit"])) {
        $update_configuration = false;


        // Check the posted old password verify with the one in the current configuration
        // and that the posted old/new password are different.
        $posted_old_password = $_POST["old_password"];
        $posted_new_password = $_POST["new_password"];
        if (password_verify($posted_old_password, PASSWORD) and $posted_old_password !== $posted_new_password) {
            $update_configuration = true;
            $password = password_hash($posted_new_password, PASSWORD_DEFAULT);
        } elseif ($update_configuration) {
            $password = PASSWORD;
        }

        // Check the posted PutAFile path is different from the one in the current configuration.
        $posted_path = $_POST["put_file_path"];
        if ($posted_path !== PUT_FILE_PATH){
            $update_configuration = true;
            $put_file_path = $posted_path;
        } elseif ($update_configuration) {
            $put_file_path = PUT_FILE_PATH;
        }

        // Check the old super password match the one in the config, and the new/old super password differs.
        $posted_super_old_password = $_POST["super_old_password"];
        $posted_super_new_password = $_POST["super_new_password"];
        if (password_verify($posted_super_old_password, PASSWORD) and $posted_super_old_password !== $posted_super_new_password) {
            $update_configuration = true;
            $super_password = password_hash($posted_super_new_password, PASSWORD_DEFAULT);
        } elseif ($update_configuration) {
            $super_password = SUPER_PASSWORD;
        }

        // Update the configuration only if changes were made.
        if ($update_configuration) {
            write_configuration($password, $put_file_path, $super_password, CONFIG_PATH);
            sleep(2);
            header("Location: /settings/");
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <?php include TEMPLATES_PATH.'/meta.php' ?>
    <title>Paramètres | PutAFile</title>

    <?php include TEMPLATES_PATH . '/css.php' ?>
</head>
<body>
    <?php include TEMPLATES_PATH.'/nav.php' ?>

    <form class="form-signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Paramètres</h1>

        <fieldset>
            <legend>PutAFile configuration</legend>
            <label for="old_password">Ancien mot de passe</label>
            <input type="password" id="old_password" name="old_password" class="form-control" autofocus>
            <label for="new_password">Nouveau mot de passe</label>
            <input type="password" id="new_password" name="new_password" class="form-control" autofocus>
            <label for="put_file_path">Chemin du fichier à télécharger</label>
            <input type="text" id="put_file_path" name="put_file_path" class="form-control"  value="<?php echo(PUT_FILE_PATH) ?>" autofocus>
        </fieldset>
        <hr>
        <fieldset>
            <legend>Super configuration</legend>
            <label for="super_old_password">Ancien mot de passe</label>
            <input type="password" id="super_old_password" name="super_old_password" class="form-control" autofocus>
            <label for="super_new_password">Nouveau mot de passe</label>
            <input type="password" id="super_new_password" name="super_new_password" class="form-control" autofocus>
        </fieldset>
        <hr>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Mettre à jour</button>
    </form>

    <?php include TEMPLATES_PATH.'/footer.php' ?>
</body>
</html>
