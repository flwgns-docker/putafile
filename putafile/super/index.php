<?php

    define('CURRENT_PATH', dirname(__FILE__));
    define('CONFIG_PATH', CURRENT_PATH.'/../../config.php');
    define('TEMPLATES_PATH', CURRENT_PATH.'/../templates');

    if (!file_exists(CONFIG_PATH)) {
        echo("PutAFile was not initialized.");
        exit(1);
    } else {
        include CONFIG_PATH;
    }

    session_start();

    if ($_SESSION["who"] === "super") {
        header("Location: /settings/");
    }

    if (isset($_POST["submit"])) {
        $super_password = $_POST["super_password"];
        if(password_verify($super_password, SUPER_PASSWORD)) {
            $_SESSION["who"] = "super";
            header("Location: /settings/");
        } else {
            echo("Credentials were not recognized.");
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <?php include TEMPLATES_PATH.'/meta.php' ?>
    <title>Connexion | PutAFile</title>

    <?php include TEMPLATES_PATH . '/css.php' ?>
</head>
<body>
    <?php include TEMPLATES_PATH.'/nav.php' ?>

    <form class="form-signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Administration</h1>
        <label for="super_password">Mot de passe</label>
        <input type="password" id="super_password" name="super_password" class="form-control" placeholder="***************" required autofocus>
        <hr>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Connexion</button>
    </form>

    <?php include TEMPLATES_PATH.'/footer.php' ?>
</body>
</html>
