<?php
    function write_configuration($password, $put_file_path, $super_password, $config_path){
        $config = "<?php\n";
        $config .= "\tdefine('PASSWORD', '" .$password. "');\n";
        $config .= "\tdefine('PUT_FILE_PATH', '" .$put_file_path. "');\n";
        $config .= "\tdefine('SUPER_PASSWORD', '" .$super_password. "');\n";
        $config .= "?>\n";

        $fh = fopen($config_path, 'w') or die("Can't open file");
        stream_set_blocking($fh, true);
        fwrite($fh, $config);
        fclose($fh);
    }

    // Force the password to have more than 10 character, with at least one lower case letter, one upper case letter and a digit.
    function validate_password($raw_password) {
        if (!preg_match('@[a-z]@', $raw_password) || !preg_match('@[A-Z]@', $raw_password) ||
            !preg_match('@[0-9]@', $raw_password) || strlen($raw_password) < 10) {
            return false;
        } else {
            return true;
        }
    }
?>