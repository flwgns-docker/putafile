<?php

    define('CURRENT_PATH', dirname(__FILE__));
    define('CONFIG_PATH', CURRENT_PATH.'/../../config.php');
    define('TEMPLATES_PATH', CURRENT_PATH.'/../templates');

    if (!file_exists(CONFIG_PATH)) {
        echo("PutAFile was not initialized.");
        exit(1);
    } else {
        include CONFIG_PATH;
    }

    session_start();

    if ($_SESSION['who']) {
        header("Location: /");
    }

    if (isset($_POST["submit"])) {
        $password = $_POST["password"];
        if(password_verify($password, PASSWORD)) {
            $_SESSION["who"] = "putafile";
            header("Location: /");
        } else {
            echo("Credentials were not recognized.");
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <?php include TEMPLATES_PATH.'/meta.php' ?>
    <title>Connexion | PutAFile</title>

    <?php include TEMPLATES_PATH . '/css.php' ?>
</head>
<body>
    <?php include TEMPLATES_PATH.'/nav.php' ?>

    <form class="form-signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal">PutAFile</h1>
        <label for="password">Mot de passe</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="***************" required autofocus>
        <hr>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Connexion</button>
    </form>

    <?php include TEMPLATES_PATH.'/footer.php' ?>
</body>
</html>
