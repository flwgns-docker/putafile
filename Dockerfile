FROM php:apache

# Set the application document root.
ENV APACHE_DOCUMENT_ROOT /var/www/html/putafile

# Copy over the application.
COPY putafile/ /var/www/html/putafile/
COPY initialise.php /var/www/html/

RUN chown -R www-data.www-data /var/www/html

# Use production php.ini, update the Apache document root.
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Extend the entrypoint to initialise the application at runtime!
RUN sed -i '/^exec "$@"$/i php initialise.php --auto' /usr/local/bin/docker-php-entrypoint