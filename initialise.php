<?php

    function show_usage() {
        echo("PutAFile initialise script\n");
        echo("\n");
        echo("Either use:\n");
        echo("\tphp initialise.php --auto\n");
        echo("\tInitialise using the following environment variables:\n");
        echo("\t\tPUTAFILE_PASSWORD\tThe password to access the application itself.\n");
        echo("\t\tPUTAFILE_PUT_FILE_PATH\tThe path used to save to put file to.\n");
        echo("\t\tPUTAFILE_SUPER_PASSWORD\tThe supe password to access the application settings.\n");
        echo("\n");
        echo("Or use:\n");
        echo("\tphp initialise.php\n");
        echo("\tInitialise with users inputs.");
        exit(1);
    }

    function run_with_user_inputs() {
        //  Configure PutAFile
        // --
        // User name must have at least 3 characters.
        echo("PutAFile configuration:\n");
        $raw_password = readline('Password? ');

        // Define the file to be put,
        // do not validate this configuration, by design, there's no point.
        $put_file_path = readline('File path to put? ');
        // --

        // Super user configuration
        echo("Super user configuration:\n");
        $raw_super_password = readline('Super password? ');

        initialise($raw_password, $put_file_path, $raw_super_password);
    }

    function run_from_env_vars() {
        if (!empty(getenv("PUTAFILE_PASSWORD")) and !empty(getenv("PUTAFILE_PUT_FILE_PATH")) and !empty(getenv("PUTAFILE_SUPER_PASSWORD"))) {
            initialise(getenv("PUTAFILE_PASSWORD"), getenv("PUTAFILE_PUT_FILE_PATH"), getenv("PUTAFILE_SUPER_PASSWORD"));
        } else {
            echo("Failed to find the PUTAFILE_PASSWORD, PUTAFILE_PUT_FILE_PATH and PUTAFILE_SUPER_PASSWORD environment variables.\n");
            echo("You have to manually initialise with `php initialise.php`.");
        }
    }

    function initialise($raw_password, $put_file_path, $raw_super_password) {
        // Define required variables.
        define('CURRENT_PATH', dirname(__FILE__));
        define('CONFIG_PATH', CURRENT_PATH.'/config.php');
        define('UTILS_PATH', CURRENT_PATH.'/putafile/assets/php/utils.php');

        // Fetch the write_configuration from the utils PHP assets.
        include UTILS_PATH;

        // Hash the passwords.
        $password = password_hash($raw_password, PASSWORD_DEFAULT);
        $super_password = password_hash($raw_super_password, PASSWORD_DEFAULT);

        // Write the configuration.
        write_configuration($password, $put_file_path, $super_password, CONFIG_PATH);
        echo("Configuration was written with ".$put_file_path." as the file put path!");
    }

    if(isset($argv[2])) {
        echo("You provided to many arguments!\n");
        show_usage();
    }

    if(isset($argv[1])) {
        if($argv[1] === "--auto") {
            run_from_env_vars();
        } else {
            echo("Could not recognise argument `".$argv[1]."``.\n");
            show_usage();
        }
    } else {
        run_with_user_inputs();
    }


?>

