# PutAFile

[![1.0.0 build status](https://gitlab.com/flwgns-docker/putafile/badges/1.0.0/pipeline.svg)](https://gitlab.com/flwgns-docker/putafile/commits/1.0.0)
[![License](https://img.shields.io/badge/License-GPLv3-red.svg)](https://www.gnu.org/licenses/quick-guide-gplv3.html)

A web application that allows you to put a remote file on your server.

## Usage

This application can be used with Docker, with docker-compose for example:

The following is from the [docker-compose.yml](docker-compose.yml) found in the repository:
```yaml
version: "3.4"

services:
  putafile:
    image: registry.gitlab.com/flwgns-docker/putafile:latest
    volumes:
      - /local/path/to/some.file:/var/www/html/some.file
    ports:
      - 80
    environment:
    - PUTAFILE_PASSWORD=somePassword
    - PUTAFILE_PUT_FILE_PATH=/var/www/html/some.file # For now, only the /var/www/html is writteable by the apache user. 
    - PUTAFILE_SUPER_PASSWORD=someOtherPasswordForTheSettingsPage
```
With the `PUTAFILE_PASSWORD`, `PUTAFILE_PUT_FILE_PATH` and `PUTAFILE_SUPER_PASSWORD`, PutAFile is initialised at the container's entrypoint.
Without those, you will have to initialise PutAFile after the container was started, with `php initialise`, which will ask you for the information passed as environment variables in this example.

The *file put* from the application is shared as a volume, *file_share* in the example, to be used by other containers.

**It's highly recommended to run this container along with a secure reverse proxy such as Treafik. Otherwise, PutAFile will be served with plain HTTP.**